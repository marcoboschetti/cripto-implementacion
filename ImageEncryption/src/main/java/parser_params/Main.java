package parser_params;

import encryption.Encryption;
import encryption.IEncryption;
import encryption.Shade;
import imageLoader.BmpImage;
import imageLoader.Parser;
import shades.ImageSaver;
import shades.ShadesLoader;
import shades.ShadesSaver;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

// Encryption examplesecret
// -d -secret resources/images/MyScrets/Audreyssd.bmp -k 8 -n 8 -dir resources/images/grupo6
// Decryption example
// -r -secret encryptionOutput/desencrypted.bmp -k 8 -n 8 -dir encryptionOutput/
// -r -secret encryptionOutput/desencrypted.bmp -k 8 -n 8 -dir encryptionOutput/group6Porters

public class Main {
    public static void main(String[] args) {
        ParserParams params = new ParserParams(args);
        File directory = new File(params.dirPath);
        File[] files = directory.listFiles();
        List<BmpImage> images = new ArrayList<>();
        List<byte[]> bodies = new ArrayList<>();
        Parser parser = new Parser();
        IEncryption encryption = new Encryption();
        for (File file : files) {
            try {
                if (!file.isDirectory() && file.getName().contains(".bmp")) {
                    // Las portadoras no se rotan
                    BmpImage image = parser.parse(file.getPath(), params.encrypt, false);
                    images.add(image);
                    bodies.add(image.getBody());
                }
            } catch (IOException e) {
                System.out.println("The file " + file.getName() + " cannot be found");
            }
        }
     //   System.out.println(images.size() + " porter images found");

        if (params.encrypt) {
            try {
                BmpImage image = parser.parse(params.secretPath, params.encrypt, true);
                List<List<Shade>> shades = encryption.getShades(params.k, params.n, image.getBody());

               //     byte[] original = encryption.getOriginalImage(params.k, params.n, shades);
               //     compareBodies(original, image.getBody());

                //printShades(shades);

                ShadesSaver shadesSaver = new ShadesSaver(shades, bodies);
                List<ImageSaver> savers = shadesSaver.save();
                for (int i = 0; i < images.size(); i++) {
                    BmpImage originalCarrier = images.get(i);
                    ImageSaver saver = savers.get(i);
                    originalCarrier.setX(saver.getX());
                    originalCarrier.setBody(saver.getImage());
                    parser.persist(originalCarrier, "encryptionOutput/" + originalCarrier.getX() + ".bmp", false);
                }

            } catch (IOException e) {
                System.out.println("La imagen a encriptar (" + params.secretPath + ") no existe");
            }
        } else {
            List<Integer> dom = new ArrayList<>();
            List<byte[]> imageList = new ArrayList<>();

            for (BmpImage bmpImage : images) {
                dom.add(bmpImage.getX());
                imageList.add(bmpImage.getBody());
            }
            ShadesLoader loader = new ShadesLoader(dom, imageList);
            List<List<Shade>> shades = loader.loadShades();

           // printShades(shades);

            byte[] originalBody = encryption.getOriginalImage(8, 8, shades);

            BmpImage aCarrirer = images.get(0);
            BmpImage originalImage = new BmpImage(aCarrirer.getHeader(), originalBody,
                   aCarrirer.getRubbish(), aCarrirer.getX(), aCarrirer.getSeed(), aCarrirer.getMetadata());

            // La imagen original se rota antes de ser guardada
            parser.persist(originalImage, params.secretPath, true);

/*
            try {
                BmpImage actualImage = parser.parse("resources/images/MyScrets/mySecret.bmp",false);
                actualImage.setBody(originalBody);
                parser.persist(actualImage, params.secretPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
 */
        }
        System.out.println("Successful execution!");
    }

    private static void compareBodies(byte[] original, byte[] body) {
        System.out.println("Sizes: " + original.length + " vs " + body.length);
        int dif = 0;
        int lastError = 0;
        for (int i = 0; i < original.length; i++) {
            if (original[i] != body[i]) {
                dif++;
                System.out.println("Diferencia en byte "+i+", hace "+(i-lastError));
                lastError = i;
            }
        }
        System.out.println("DIFS: " + dif);
    }

    private static void printShades(List<List<Shade>> shades) {
        try {
            PrintWriter writer = new PrintWriter("ShadesOutput"+((int)(Math.random()*1000)));
            for (int i = 0; i < shades.size(); i++) {
                List<Shade> shadeList = shades.get(i);
                for (Shade s : shadeList) {
                    writer.write("(" + s.getX() + " | " + s.getY() + ")\n");
                    if(i == 0){
                        System.out.println("(" + s.getX() + " | " + s.getY() + ")");
                    }
                }
            }
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
