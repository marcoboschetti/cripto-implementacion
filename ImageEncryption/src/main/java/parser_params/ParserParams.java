package parser_params;

public class ParserParams {
    // true for encrypt, false for decrypt
    boolean encrypt;
    String secretPath;
    int k;
    int n;
    String dirPath;

    public ParserParams (String[] args) {
        if(args.length < 5){
            throw new IllegalArgumentException("Se esperan al menos los 3 parámetros obligatorios");
        }

        if (args[0].equals("-d")) {
            encrypt = true;
        } else if (args[0].equals("-r")) {
            encrypt = false;
        } else {
            throw new IllegalArgumentException("Parametro invalido, las unicas acciones posibles son -d y -r");
        }

        if (!args[1].equals("-secret")) {
            throw new IllegalArgumentException("Parametro invalido, se espera un -secret");
        }

        secretPath = args[2];

        if (!args[3].equals("-k")) {
            throw new IllegalArgumentException("Parametro invalido, se espera un -k");
        }

        k = Integer.parseInt(args[4]);
        if (k < 2) {
            throw new IllegalArgumentException("k debe ser mayor o igual a 2");
        }
        dirPath = "";
        n = 0;

        if (args.length > 5) {
            if (args.length == 7) {
                if (args[5].equals("-n")) {
                    n = Integer.parseInt(args[6]);
                } else if (args[5].equals("-dir")) {
                    dirPath = args[6];
                } else {
                    throw new IllegalArgumentException("Parametro invalido, se espera un -n o un -dir");
                }
            } else if (args.length == 9) {
                if (!args[5].equals("-n") || !args[7].equals("-dir")) {
                    throw new IllegalArgumentException("Parametro invalido, se espera un -n y un -dir");
                }
                n = Integer.parseInt(args[6]);
                dirPath = args[8];

            } else {
                throw new IllegalArgumentException("Cantidad de parámetros inválida.");
            }

        }
    }
}
