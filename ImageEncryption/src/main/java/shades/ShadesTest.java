package shades;

import encryption.Encryption;
import encryption.IEncryption;
import encryption.Shade;

import java.util.ArrayList;
import java.util.List;

public class ShadesTest {

/*    public static void main(String[] args) {
        List<Shade> shades1 = new ArrayList<>();
        shades1.add(new Shade(1,4));
        shades1.add(new Shade(1,42));
        shades1.add(new Shade(1,62));
        shades1.add(new Shade(1,231));

        List<Shade> shades2 = new ArrayList<>();
        shades2.add(new Shade(3,123));
        shades2.add(new Shade(3,54));
        shades2.add(new Shade(3,91));
        shades2.add(new Shade(3,145));

        List<Shade> shades3 = new ArrayList<>();
        shades3.add(new Shade(51,143));
        shades3.add(new Shade(51,62));
        shades3.add(new Shade(51,222));
        shades3.add(new Shade(51,3));

        List<Shade> shades4 = new ArrayList<>();
        shades4.add(new Shade(123,13));
        shades4.add(new Shade(123,23));
        shades4.add(new Shade(123,200));
        shades4.add(new Shade(123,45));

        List<List<Shade>> shadesList = new ArrayList<>();
        shadesList.add(shades1);
        shadesList.add(shades2);
        shadesList.add(shades3);
        shadesList.add(shades4);
    }*/

    public static void main(String[] args) {
        testBiyectiveProperty(8);
    }

    private static void testBiyectiveProperty(int imageTotalSize) {

        IEncryption encryption = new Encryption();
        byte[] originalImage = new byte[imageTotalSize];
        for(int i = 0; i < imageTotalSize; i++){
            originalImage[i] = (byte)(Math.random() * Encryption.GAUSS_FIELD_SIZE);
        }
        List<List<Shade>> shades = encryption.getShades(8,8,originalImage);
        printShades(shades);
        List<byte[]> carrierImages = new ArrayList<>();
        carrierImages.add(getRandomImage(imageTotalSize));
        carrierImages.add(getRandomImage(imageTotalSize));
        carrierImages.add(getRandomImage(imageTotalSize));
        carrierImages.add(getRandomImage(imageTotalSize));
        carrierImages.add(getRandomImage(imageTotalSize));
        carrierImages.add(getRandomImage(imageTotalSize));
        carrierImages.add(getRandomImage(imageTotalSize));
        carrierImages.add(getRandomImage(imageTotalSize));



        ShadesSaver shadesSaver = new ShadesSaver(shades, carrierImages);
        shadesSaver.save();

        List<Integer> xList = new ArrayList<>();
        xList.add(1);
        xList.add(2);
        xList.add(3);
        xList.add(4);
        xList.add(5);
        xList.add(6);
        xList.add(7);
        xList.add(8);

        ShadesLoader shadesLoader = new ShadesLoader(xList, carrierImages);
        List<List<Shade>> shades1 = shadesLoader.loadShades();
        System.out.println("-------------------");

        printShades(shades1);

        byte[] reconstructedImage = encryption.getOriginalImage(8,8,shades1);

        for(int i = 0; i < originalImage.length; i++){
            if(originalImage[i] != reconstructedImage[i]){
                throw new IllegalStateException("Wrong pixel recovered: "+originalImage[i]+" vs "+reconstructedImage[i]);
            }
        }
        System.out.println("TODO OK! =D");

    }

    private static byte[] getRandomImage(int dim) {
        byte[] originalImage = new byte[dim];
        for(int i = 0; i < dim; i++){
            originalImage[i] = (byte)(Math.random() * Encryption.GAUSS_FIELD_SIZE);
        }
        return originalImage;
    }

    private static void printShades(List<List<Shade>> shades) {
        for (List<Shade> shadeList : shades) {
            for (Shade shade : shadeList) {
                System.out.println("(" + (shade.getX() & 0xFF) + "," + (shade.getY() & 0xFF) + ")");
            }
        }
    }
}
