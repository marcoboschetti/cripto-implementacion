package shades;

import java.util.ArrayList;
import encryption.*;
import java.util.List;

public class ShadesSaver {
    List<List<Shade>> shadesList;
    List<ImageSaver> imageSaverList = new ArrayList<>();


    public ShadesSaver(List<List<Shade>> shadesList, List<byte[]> images) {
        this.shadesList = shadesList;
        for (byte[] bytes : images) {
            imageSaverList.add(new ImageSaver(bytes));
        }
        if (images.size() != shadesList.get(0).size()) {
            throw new IllegalArgumentException("LOS TAMAÑOS NO COINCIDEN --> SHADESSAVER");
        }
    }

    public List<ImageSaver> save() {
        for(int i = 0; i < shadesList.size(); i++) {
            for(int j = 0; j < shadesList.get(i).size(); j++) {
                if (i == 0) {
                    imageSaverList.get(j).setX(shadesList.get(i).get(j).getX());
                }
                Shade currentShade = shadesList.get(i).get(j);
                imageSaverList.get(j).saveShade((byte)currentShade.getY());
            }
        }
        return imageSaverList;
    }

}
