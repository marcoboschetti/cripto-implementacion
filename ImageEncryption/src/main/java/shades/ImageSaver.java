package shades;

public class ImageSaver {
    byte[] image;
    int index;
    int X;


    public ImageSaver(byte[] image) {
        this.image = image;
        this.index = 0;
    }

    public void saveShade(byte byteToSave) {
        for (int i = 8; i > 0; i--) {
            byte imageByte = image[index + 8 - i];
            imageByte = (byte)(imageByte & 0XFE);
            byte current = (byte)(0X01 << (i-1));
            byte byteAux = (byte) ((byte)(byteToSave & current) >> (i-1));
            byteAux = (byte) (byteAux & 0x01);
            imageByte = (byte)(imageByte | byteAux);
            image[index + 8 - i] = imageByte;
        }
        index += 8;
    }

    public byte[] getImage() {
        return image;
    }

    public int getX() {
        return X;
    }

    public void setX(int x) {
        X = x;
    }
}
