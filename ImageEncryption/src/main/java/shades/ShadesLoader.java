package shades;

import encryption.Shade;

import java.util.ArrayList;
import java.util.List;

public class ShadesLoader {
    List<Integer> xShades;
    List<byte[]> carrierImages;

    public ShadesLoader(List<Integer> dom, List<byte[]> carrierImages) {
        this.xShades = dom;
        this.carrierImages = carrierImages;
    }

    public List<List<Shade>> loadShades() {
        List<List<Shade>> shadesList = new ArrayList<>();
        int index = 0;
        while (index < carrierImages.get(0).length) {
            List<Shade> shades = new ArrayList<>();
            for (int i = 0; i < xShades.size(); i++) {
                byte[] image = carrierImages.get(i);
                int x = xShades.get(i);
                byte currentByte;
                byte yShade = 0;
                for (int j = 0; j < 8; j++) {
                    yShade<<=1;
                    currentByte = (byte)(image[index + j] & 0X01);
                    yShade = (byte)(yShade | currentByte);
                }
                shades.add(new Shade(x, (yShade & 0xFF)));
            }
            shadesList.add(shades);
            index += 8;
        }
        return shadesList;
    }



}
