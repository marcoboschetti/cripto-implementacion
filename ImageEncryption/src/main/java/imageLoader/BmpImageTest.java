package imageLoader;

import java.io.File;
import java.io.IOException;

public class BmpImageTest {

    public static void main(String[] args) {
        Parser parser = new Parser();
        String path = "resources/images/secretless/Alfred.bmp";
        try {
            System.out.println(new File(path).exists());
            BmpImage image = parser.parse(path, true, true);
            System.out.println(image.getBody().length);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
