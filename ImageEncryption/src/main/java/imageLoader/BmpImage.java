package imageLoader;

/**
 * Created by jcl on 6/23/17.
 */
public class BmpImage {
    final private byte[] header;
    private byte[] body;
    final private byte[] rubbish;
    private final int seed;
    private final byte[] metadata;
    private int x;

    public BmpImage(byte[] header, byte[] body, byte[] rubbish, int x, int seed, byte[] metadata) {
        this.header = header;
        this.body = body;
        this.x = x;
        this.seed = seed;
        this.rubbish = rubbish;
        this.metadata = metadata;
    }

    public void setBody(byte[] body) {
        this.body = body;
    }

    public byte[] getBody() {
        return body;
    }

    public int getSeed() {
        return seed;
    }

    public byte[] getMetadata() {
        return metadata;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public byte[] getHeader() {
        return header;
    }

    public byte[] getRubbish() {
        return rubbish;
    }


}
