package imageLoader;

import encryption.Encryption;
import encryption.Shade;
import shades.ShadesLoader;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MultipleImageTest {

    public static void main(String[] args) {
        Parser parser = new Parser();
        List<String> sourcePaths = new LinkedList<>();
        sourcePaths.add("resources/images/customTest/Angelinassd.bmp");
        sourcePaths.add("resources/images/customTest/Gracessd.bmp");
        sourcePaths.add("resources/images/customTest/Jimssd.bmp");
        sourcePaths.add("resources/images/customTest/Lizssd.bmp");
        sourcePaths.add("resources/images/customTest/Robertossd.bmp");
        sourcePaths.add("resources/images/customTest/Shakirassd.bmp");
        sourcePaths.add("resources/images/customTest/Susanassd.bmp");
        sourcePaths.add("resources/images/customTest/Whitneyssd.bmp");

        List<BmpImage> images = new LinkedList<>();
        try {
            for (String s : sourcePaths) {
                BmpImage image = parser.parse(s, false, false);
                images.add(image);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<Integer> dom = new ArrayList<>();
        List<byte[]> imageList = new ArrayList<>();
        for (BmpImage bmpImage : images) {
            dom.add(bmpImage.getX());
            imageList.add(bmpImage.getBody());
        }

        ShadesLoader loader = new ShadesLoader(dom, imageList);
        List<List<Shade>> shades = loader.loadShades();

        Encryption encryption = new Encryption();
        byte[] originalBody = encryption.getOriginalImage(8, 8, shades);

        BmpImage aCarrirer = images.get(2);

        BmpImage originalImage = new BmpImage(aCarrirer.getHeader(), originalBody,
                aCarrirer.getRubbish(), aCarrirer.getX(), aCarrirer.getSeed(), aCarrirer.getMetadata());

        parser.persist(originalImage, "decripted.bmp", true);
    }

}
