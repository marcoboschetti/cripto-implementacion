package imageLoader;

import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ParserTest {

    @Test
    public void parse() {
        Parser parser = new Parser();
        String path = "resources/images/Angelinassd.bmp";
        String path2 = "resources/images/customTest/Gracessd.bmp";
        try {
            BmpImage image = parser.parse(path, true, false);
            BmpImage image2 = parser.parse(path2, true, false);
            image2.setBody(image.getBody());
            parser.persist(image, "out/output2.bmp", false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test(expected = FileNotFoundException.class)
    public void notExistantFile() throws IOException {
        Parser parser = new Parser();
        String path = "fakepath/fakeImage.bmp";

        BmpImage image = parser.parse(path, true, false);

    }
}
