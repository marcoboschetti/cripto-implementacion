package imageLoader;

import com.sun.xml.messaging.saaj.util.ByteOutputStream;

import java.io.*;
import java.util.Random;

public class Parser {
    private static final int MAX_BYTE_VALUE = 256;
    private static final int SEED = (int)(Math.random() * (MAX_BYTE_VALUE * MAX_BYTE_VALUE));
    private final int HEADER_SIZE = 54;
    private final int BUFFER_SIZE = 8;
    private final int SEED_INDEX = 6;
    private final int X_INDEX = 8;
    private final Random rnd;

    public Parser() {
        rnd = new Random();
    }

    public BmpImage parse(String path, boolean encrypt, boolean applyRotation) throws IllegalStateException, IOException {
//        InputStream is = getClass().getResourceAsStream(path);
        File file = new File(path);
        if (!file.exists()) {
            throw new FileNotFoundException("File does not exist.");
        }
        InputStream is = new FileInputStream(file);
        byte[] header = new byte[HEADER_SIZE];
        int seed;
        try {
            if (is.read(header) != HEADER_SIZE) {
                throw new IllegalStateException("Not a valid image.");
            }
        } catch (IOException e) {
            throw new IOException("Error reading the file.");
        }
        if (encrypt) {
            rnd.setSeed((SEED & 0xFFFF));
            header[6] = (byte) ((SEED & 0xFFFF) % 256);
            header[7] = (byte) (((SEED & 0xFFFF) / 256) & 0xFF);
            seed = (SEED & 0xFFFF);
          //  System.out.println("escribo seed:"+seed);
        } else {
            seed = getLittleEndianValue2(header, 6);
            rnd.setSeed((seed & 0xFFFF));
            //   System.out.println("leo seed: " + seed);
        }
        int bodyOffset = getLittleEndianValue4(header, 10);
        int imageSize = getLittleEndianValue4(header, 18) * getLittleEndianValue4(header, 22);
      //  System.out.println("dimensiones: "+getLittleEndianValue4(header, 18)+"x"+getLittleEndianValue4(header, 22));
        byte[] rubbish = new byte[bodyOffset - HEADER_SIZE];
        is.read(rubbish);
        byte[] buffer = new byte[BUFFER_SIZE];
        ByteOutputStream os = new ByteOutputStream();
        int restSize;
        int added = 0;

        try {
            while ((restSize = is.read(buffer)) == BUFFER_SIZE && added <= (imageSize - BUFFER_SIZE)) {
                for (int i = 0; i < BUFFER_SIZE; i++) {
                    if(applyRotation) {
                        buffer[i] = (byte) (buffer[i] ^ (byte) rnd.nextInt(MAX_BYTE_VALUE));
                    }
                }
                os.write(buffer);
                added += BUFFER_SIZE;
            }
            byte[] body = os.toByteArray();
            os.reset();
            if (restSize != -1) {
                os.write(buffer, 0, restSize);
            }
            while ((restSize = is.read(buffer)) != -1) {
                os.write(buffer, 0, restSize);
            }
            byte[] metadata = os.toByteArray();
            return new BmpImage(
                    header,
                    body,
                    rubbish,
                    getLittleEndianValue2(header, 8),
                    seed,
                    metadata
            );
        } catch (IOException e) {
            throw new IOException("Error reading the file.");
        }
    }

    public void persist(BmpImage image, String path, boolean applyRotation) {
        File file = new File(path);
        try {
            OutputStream fos = new FileOutputStream(file);
            byte[] header = image.getHeader();
            header[6] = (byte) (image.getSeed() % 256);
            header[7] = (byte) (image.getSeed() / 256);
            header[8] = (byte) (image.getX() % 256);
            header[9] = (byte) (image.getX() / 256);

            fos.write(header);
            byte[] body = image.getBody();
            Random random = new Random();
            random.setSeed(image.getSeed());
            fos.write(image.getRubbish());
            if(applyRotation){
                for (int i = 0; i < body.length; i++) {
                    body[i] ^= (byte) random.nextInt(MAX_BYTE_VALUE);
                }
            }
            fos.write(body);
            fos.write(image.getMetadata());
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int getLittleEndianValue4(byte[] input, int start) {
       // return ((input[start + 3] * MAX_BYTE_VALUE + input[start+2])
       //         * MAX_BYTE_VALUE + input[start + 1]) * MAX_BYTE_VALUE + input[start];
        int lowTerm = getLittleEndianValue2(input, start);
        int highTerm = getLittleEndianValue2(input, start+2);
        return highTerm * MAX_BYTE_VALUE * MAX_BYTE_VALUE + lowTerm;
    }

    private int getLittleEndianValue2(byte[] input, int start) {
        return (input[start + 1] & 0xff) * MAX_BYTE_VALUE + (input[start] & 0xff);
    }

    private byte[] getRandoms(int length, int seed) {
        rnd.setSeed(seed);
        byte[] ret = new byte[length];
        for (int i = 0; i < length; i++) {
            ret[i] = (byte) rnd.nextInt(MAX_BYTE_VALUE);
        }
        return ret;
    }
}
