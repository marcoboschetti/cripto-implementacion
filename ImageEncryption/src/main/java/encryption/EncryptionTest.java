package encryption;

import java.util.LinkedList;
import java.util.List;

public class EncryptionTest {

    public static void main(String[] args) {
        testSimpleEncryptNoBorderCases();
        testComplexEncryptNoBorderCases();

        testBiyectiveProperty(18);
    }

    private static void testBiyectiveProperty(int imageTotalSize) {

        IEncryption encryption = new Encryption();
        byte[] originalImage = new byte[imageTotalSize];
        for (int i = 0; i < imageTotalSize; i++) {
            originalImage[i] = (byte) (Math.random() * Encryption.GAUSS_FIELD_SIZE);
        }
        List<List<Shade>> shades = encryption.getShades(8, 8, originalImage);
        byte[] reconstructedImage = encryption.getOriginalImage(8, 8, shades);

        for (int i = 0; i < originalImage.length; i++) {
            if (originalImage[i] != reconstructedImage[i]) {
                throw new IllegalStateException("Wrong pixel recovered: " + originalImage[i] + " vs " + reconstructedImage[i]);
            }
        }
        System.out.println("TODO OK! =D");

    }

    public static void testSimpleEncryptNoBorderCases() {
        IEncryption encryption = new Encryption();
        byte[] originalImage = new byte[]{71, 44, 52, 37, 91, (byte) (221 & 0xFF), 67, (byte) (177 & 0xFF)};
        int n = 8;
        int r = 8;
        //Mod[(71+44*x+52*x^2+37*x^3+91*x^4+221*x^5+67*x^6+177*x^7),257]
        List<List<Shade>> expectedShades = new LinkedList<>();
        List<Shade> expectedFirstGroupShade = new LinkedList<>();
        expectedFirstGroupShade.add(0, new Shade(1, 246));
        expectedFirstGroupShade.add(1, new Shade(2, 155));
        expectedFirstGroupShade.add(2, new Shade(3, 106));
        expectedFirstGroupShade.add(3, new Shade(4, 95));
        expectedFirstGroupShade.add(4, new Shade(5, 32));
        expectedFirstGroupShade.add(5, new Shade(6, 247));
        expectedFirstGroupShade.add(6, new Shade(7, 253));
        expectedFirstGroupShade.add(7, new Shade(8, 168));
        expectedShades.add(0, expectedFirstGroupShade);

        List<List<Shade>> shades = encryption.getShades(r, n, originalImage);

        checkShadeLists(shades, expectedShades, "Basic encryption");
    }

    /**
     * This test checks the scenario where the polynomial value is 256.
     * In this case, the encrypter should reduce the first coefficient (81) by 1 (80)
     * and recalculate all the shades with that new value. The sames happens for the value 6 of that polynomial.
     * So the last used polynomial is 79.
     */
    public static void testComplexEncryptNoBorderCases() {
        IEncryption encryption = new Encryption();
        byte[] originalImage = new byte[]{81, 44, 52, 37, 91, (byte) (221 & 0xFF), 67, (byte) (177 & 0xFF)};
        int n = 8;
        int r = 8;
        //Mod[(81+44*x+52*x^2+37*x^3+91*x^4+221*x^5+67*x^6+177*x^7),257]
        List<List<Shade>> expectedShades = new LinkedList<>();
        List<Shade> expectedFirstGroupShade = new LinkedList<>();
        expectedFirstGroupShade.add(0, new Shade(1, 254));
        expectedFirstGroupShade.add(1, new Shade(2, 163));
        expectedFirstGroupShade.add(2, new Shade(3, 114));
        expectedFirstGroupShade.add(3, new Shade(4, 103));
        expectedFirstGroupShade.add(4, new Shade(5, 40));
        expectedFirstGroupShade.add(5, new Shade(6, 255));
        expectedFirstGroupShade.add(6, new Shade(7, 4));
        expectedFirstGroupShade.add(7, new Shade(8, 176));
        expectedShades.add(0, expectedFirstGroupShade);

        List<List<Shade>> shades = encryption.getShades(r, n, originalImage);

        checkShadeLists(shades, expectedShades, "Complex encryption");
    }


    private static void checkShadeLists(List<List<Shade>> shades, List<List<Shade>> expectedShades, String test) {
        if (shades.size() != expectedShades.size()) {
            throw new IllegalStateException(test + ": Sizes dont match");
        }

        for (int i = 0; i < shades.size(); i++) {
            List<Shade> shadeList = shades.get(i);
            List<Shade> expectedShadeList = expectedShades.get(i);
            for (int j = 0; j < shadeList.size(); j++) {
                Shade shade = shadeList.get(j);
                Shade expectedShade = expectedShadeList.get(j);

                if (shade.getX() != expectedShade.getX()) {
                    throw new IllegalStateException(test + ": Unexpected shade order ");
                }
                if (shade.getY() != expectedShade.getY()) {
                    throw new IllegalStateException(test + ": Incorrect shade value");
                }
            }
        }

        System.out.println(test + " works wonderfully!");
    }
}
