package encryption;

import java.util.List;

public interface IEncryption {

    /**
     * Returns the shades for the original image in the given (r,n) schema
     * @return
     *
     * The returned list contains a list for each group of pixels.
     * Being "shades" the returned value, shades[0] gives all the shades for the first polynomial.
     * For example, shades[0][0] goes into the first position of the first porter image.
     *              shades[0][1] goes into the first position of the second porter image.
     *              shades[1][0] goes into the second position of the first porter image.
     *
     */
    List<List<Shade>> getShades(int r, int n, byte[] originalImage);

    /**
     * @param shades is the same structure as returned in the "getShades" method
     */
    byte[] getOriginalImage(int r, int n, List<List<Shade>> shades);



}
