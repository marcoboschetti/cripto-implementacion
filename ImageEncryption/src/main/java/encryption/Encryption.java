package encryption;

import encryption.gaussianSolver.Matrix;
import encryption.gaussianSolver.PrimeField;

import java.util.*;

public class Encryption implements IEncryption {
    public static final int GAUSS_FIELD_SIZE = 257;

    @Override
    public List<List<Shade>> getShades(int r, int n, byte[] originalImage) {
        this.validateParameters(r, originalImage);

        List<List<Shade>> fullShades = new ArrayList<>();
        for (int pixelGroup = 0; pixelGroup < (originalImage.length / r); pixelGroup++) {
            byte[] coefficients = Arrays.copyOfRange(originalImage, pixelGroup * r, (pixelGroup + 1) * r);
            List<Shade> groupShades = this.getShadesForGroup(coefficients, n);
            fullShades.add(pixelGroup, groupShades);
        }

        return fullShades;
    }

    private void validateParameters(int r, byte[] originalImage) {
        if (originalImage.length % r != 0) {
            throw new IllegalArgumentException("The original image size should be multiple of r");
        }
    }

    private List<Shade> getShadesForGroup(byte[] coefficients, int n) {
        List<Shade> shades = new ArrayList<>();

        for (int x = 1; x <= n; x++) {
            int y = getShadeImage(coefficients, x);
            if (y != GAUSS_FIELD_SIZE - 1) {
                shades.add(x - 1, new Shade(x, y));
            } else {
                decreaseFirstNonZeroCoef(coefficients);
                shades.clear();
                x = 0;
            }
        }
        return shades;
    }

    private void decreaseFirstNonZeroCoef(byte[] coefficients) {
        int firstNonZeroIndex = 0;
        while (coefficients[firstNonZeroIndex] == 0) {
            firstNonZeroIndex++;
            if (firstNonZeroIndex >= coefficients.length) {
                throw new IllegalStateException("All the pixels in the group are 0, or all the polynomails give " +
                        (GAUSS_FIELD_SIZE - 1));
            }
        }
        coefficients[firstNonZeroIndex] -= 1;
    }

    private int getShadeImage(byte[] coefficients, int x) {
        int y = 0;
        for (int i = 0; i < coefficients.length; i++) {
            y += Math.pow(x, i) * (coefficients[i] & 0xFF);
            y = y % GAUSS_FIELD_SIZE;
        }
        return y % GAUSS_FIELD_SIZE;
    }


    @Override
    public byte[] getOriginalImage(int r, int n, List<List<Shade>> shades) {
        List<Byte> totalPixels = new LinkedList<>();

        for(int groupCount = 0; groupCount < shades.size(); groupCount++){
            List<Shade> shadeGroup  = shades.get(groupCount);
            if(shadeGroup.size() < r){
                throw new IllegalStateException("I need at least "+r+" shades, and I got "+shades.size());
            }

            List<Byte> groupPixels = this.getPolynomialFromShades(r, n, shadeGroup);
            totalPixels.addAll(groupPixels);
        }

        byte[] image = new byte[totalPixels.size()];

        for(int i = 0; i < totalPixels.size(); i++){
            image[i] = totalPixels.get(i);
        }
        return image;
    }

    private List<Byte> getPolynomialFromShades(int r, int n, List<Shade> shadeGroup) {
        PrimeField field = new PrimeField(GAUSS_FIELD_SIZE);
        Matrix<Integer> mat = new Matrix<>(shadeGroup.size(), r+1, field);

        for (int shadeCont = 0; shadeCont < shadeGroup.size(); shadeCont++) {
            Shade s = shadeGroup.get(shadeCont);
            for (int coeff = 0; coeff < r; coeff++) {
                mat.set(shadeCont,coeff, (int)( Math.pow((s.getX() & 0xFF), coeff) % GAUSS_FIELD_SIZE));
            }
            mat.set(shadeCont, r, (s.getY() & 0xFF));
        }

        // Gauss-Jordan elimination
        mat.reducedRowEchelonForm();

        List<Byte> pixels = new ArrayList<>();
        // Print resulting matrix
        for (int i = 0; i < r; i++) {
            byte pixel = mat.get(i,r).byteValue();
            pixels.add(i,pixel);
        }

        return pixels;
    }
}
