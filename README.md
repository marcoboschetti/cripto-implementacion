# README #

La aplicación utiliza un algoritmo de Esquema de Umbral (k,n) para obtener sombras a partir de una imagen original, y un proceso de esteganografía para guardar dichas sombras en imágenes portadoras, sin que la modificación de estas resulte evidente a simple vista

### Instalación ###

Para compilar la aplicación, se debe clonar el repositorio, ubicar la terminal en el directorio raíz y ejecutar el comando:


```
mvn clean package


```



Con esta instrucción, se descargaran todas las dependencias necesarias para que la aplicación se ejecute correctamente, y luego compilará la misma.
Por defecto, el archivo compilado se encuentra en **cripto-implementacion/ImageEncryption/target**.
Para ejecutar el programa, se debe identificar el archivo .jar generado (ImageEncryption-1.0-SNAPSHOT-jar-with-dependencies.jar) y ejecutar el comando:
```
java -jar ImageEncryption-1.0-SNAPSHOT-jar-with-dependencies <ARGS>

```
donde el valor de <ARGS> es el provisto en el enunciado del presente trabajo.




### Ejemplos de ejecución ###

Con el objetivo de faciliar la ejecución de este programa, se incluyó una serie de imágenes pequeñas. Si se mueve el archivo "ImageEncryption-1.0-SNAPSHOT-jar-with-dependencies.jar" al directorio raíz del proyecto, puede ejecutarse correctamente la siguiente instrucción:

```
java -jar ImageEncryption-1.0-SNAPSHOT-jar-with-dependencies.jar -d -secret resources/images/MyScrets/Audreyssd.bmp -k 8 -n 8 -dir resources/images/grupo6

```

con lo cual el programa distribuirá la imagen secreta en las 8 imágenes portadoras ubicadas en un directorio de recursos. Como objetivo de esta ejecución exitosa, se espera ver en el directorio **encryptionOutput** 8 imágenes nombradas "X.bmp" donde X va de 1 a 8.
Para recuperar la imagen original, basta con ejecutar la siguiente instrucción:
```
java -jar ImageEncryption-1.0-SNAPSHOT-jar-with-dependencies.jar -r -secret encryptionOutput/desencrypted.bmp -k 8 -n 8 -dir encryptionOutput/


```

Como resultado exitoso de esta ejecución, debe generarse el archivo **desencrypted.bmp** el el directorio **encryptionOutput**, el cual contiene la imagen secreta original.




****